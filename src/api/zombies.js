import { request } from "../config/axios.config";
import * as mock from './mock/zombies'
import * as env from '../config/env.config'

const listZombies = async (pageInfo) => {
    return env.ENV === env.pro ? request.get('/zombies/page', {
        params: {
            currentPage: pageInfo.current || 1,
            pageSize: pageInfo.size || 10,
        }
    }) : mock.listZombies()
}

const zombieInfo = async () => {
    return env.ENV === env.pro ? request.get('http://httpbin.org/get') : mock.zombieInfo()
}

export {
    listZombies,
    zombieInfo
}
import { request } from "../config/axios.config";
import * as mock from './mock/user'
import * as env from '../config/env.config'

const dataPreprocess = data => {
    data.rows.forEach(it => {
        it.type = it.type == 0 ? "普通用户" : "管理员用户"
        it.status = it.status == 0 ? "启用" : "禁用"
    })
    return data
}

const listAdmin = async (pageInfo) => {
    const { data } = await request.get('/user/page', {
        params: {
            currentPage: pageInfo.current || 1,
            pageSize: pageInfo.size || 10,
            type: 1
        }
    })
    return dataPreprocess(data)
}

const listUser = async (pageInfo) => {
    const { data } = await request.get('/user/page', {
        params: {
            currentPage: pageInfo.current || 1,
            pageSize: pageInfo.size || 10,
            type: 0
        }
    })
    return dataPreprocess(data)
}

const addUser = (user) => {
    const nUser = {}
    Object.assign(nUser, user)
    nUser.status = user.status == '启用' ? 0 : 1
    nUser.type = user.type == '普通用户' ? 0 : 1
    return request.post('/user/add', {
        ...nUser
    })
}

const editUser = (user) => {
    const nUser = {}
    Object.assign(nUser, user)
    nUser.status = user.status == '启用' ? 0 : 1
    nUser.type = user.type == '普通用户' ? 0 : 1
    return request.post('/user/update', {
        ...nUser
    })
}

const delUser = async (uid) => {
    return request.get(`/user/del/${uid}`)
}

const delUserBatch = async (uids) => {
    await uids.forEach(it => {
        request.get(`/user/del/${it}`)
    })
}


export {
    listAdmin,
    listUser,
    addUser,
    editUser,
    delUser,
    delUserBatch
}
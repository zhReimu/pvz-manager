interface IUser {
    uname: string,
    password: string,
    mail: string,
    sex: string,
    phone: string
}

interface IResponse<T> {
    code: number,
    data: T,
    msg: string
}

interface ITeamMember {
    name: string,
    pic: string,
    mail: string
}

interface IPlants {
    pid: number,
    name: string,
    suns: number,
    cd: number,
    atk: number,
    hp: number,
    status: string,
    env: string,
    img: string,
    description: string
}

interface IZombies {
    zid: number,
    name: string,
    atk: number,
    hp: number,
    img: string,
    description: string
}

export type {
    IUser,
    IResponse,
    ITeamMember,
    IPlants,
    IZombies
}
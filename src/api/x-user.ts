import { request } from "../config/axios.config";
import { IUser, IResponse } from "./x-type";

const register = async (user: IUser): Promise<IResponse<unknown>> => {
    return request.post('/user/register', user)
}

export {
    register
}
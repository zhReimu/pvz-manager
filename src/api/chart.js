import { request } from "../config/axios.config";
import * as mock from './mock/user'
import * as env from '../config/env.config'

const userDetails = () => {
    return request.get('/chart/userDetails')
}
const plantDetails = () => {
    return request.get('/chart/plantDetails')
}
const zombieDetails = () => {
    return request.get('/chart/zombieDetails')
}

export {
    userDetails,
    plantDetails,
    zombieDetails
}
import { request } from "../config/axios.config";
import { IResponse } from "./x-type";

const delZombie = (pid: number) => {
    return request.get(`/zombies/del/${pid}`) as Promise<IResponse<string>>
}

export {
    delZombie
}
import { request } from "../config/axios.config";
import { IResponse, IPlants, IZombies } from "./x-type";

const listPlants = () => {
    return request.get('/plants/listPlants') as Promise<IResponse<IPlants[]>>
}

const listZombies = () => {
    return request.get('/zombies/listZombies') as Promise<IResponse<IZombies[]>>
}

export {
    listPlants,
    listZombies
}
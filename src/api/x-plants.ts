import { request } from "../config/axios.config";
import { IResponse } from "./x-type";

const delPlant = (pid: number) => {
    return request.get(`/plants/del/${pid}`) as Promise<IResponse<string>>
}

export {
    delPlant
}
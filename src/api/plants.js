import { request } from "../config/axios.config";
import * as mock from './mock/plants'
import * as env from '../config/env.config'

const listPlants = async (pageInfo) => {
    return env.ENV === env.pro ? request.get('/plants/page', {
        params: {
            currentPage: pageInfo.current || 1,
            pageSize: pageInfo.size || 10,
        }
    }) : mock.listPlants()
}

const updatePlant = (plant) => {
    return request.post('/plants/updatePlants', {
        ...plant
    })
}

const plantInfo = async () => {
    return env.ENV === env.pro ? request.get('http://httpbin.org/get') : mock.plantInfo()
}

export {
    listPlants,
    plantInfo,
    updatePlant
}
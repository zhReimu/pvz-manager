
const listZombies = async () => {
    return [
        {
            zid: 1,
            name: '普通僵尸',
            atk: 123,
            hp: 456,
            img: 'https://www.dmoe.cc/random.php',
            description: '普通僵尸哒'
        },
        {
            zid: 2,
            name: '普通僵尸',
            atk: 123,
            hp: 456,
            img: 'https://www.dmoe.cc/random.php',
            description: '普通僵尸哒'
        },
        {
            zid: 3,
            name: '普通僵尸',
            atk: 123,
            hp: 456,
            img: 'https://www.dmoe.cc/random.php',
            description: '普通僵尸哒'
        },
        {
            zid: 4,
            name: '普通僵尸',
            atk: 123,
            hp: 456,
            img: 'https://www.dmoe.cc/random.php',
            description: '普通僵尸哒'
        },
        {
            zid: 5,
            name: '普通僵尸',
            atk: 123,
            hp: 456,
            img: 'https://www.dmoe.cc/random.php',
            description: '普通僵尸哒'
        }
    ]
}

const zombieInfo = async () => {
    return {
        num: 132
    }
}


export {
    listZombies,
    zombieInfo
}
const login = async (username, password) => {
    return {
        success: true
    }
}

const whoami = async () => {
    return {
        username: 'admin'
    }
}

const setLogged = () => {
    sessionStorage.setItem('logged', true)
}

const getLogged = () => {
    return sessionStorage.getItem('logged')
}

const logout = () => {
    sessionStorage.removeItem('logged')
}

export {
    login,
    whoami,
    logout,
    setLogged,
    getLogged
}
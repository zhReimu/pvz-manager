const listPlants = async () => {
    return [
        {
            pid: 1,
            name: '豌豆射手',
            suns: 100,
            cd: 8,
            atk: 20,
            hp: 300,
            status: 0,
            env: 'day',
            img: 'https://www.dmoe.cc/random.php',
            description: '豌豆射手哒'
        },
        {
            pid: 2,
            name: '豌豆射手',
            suns: 100,
            cd: 8,
            atk: 20,
            hp: 300,
            status: 0,
            env: 'day',
            img: 'https://www.dmoe.cc/random.php',
            description: '豌豆射手哒'
        },
        {
            pid: 3,
            name: '豌豆射手',
            suns: 100,
            cd: 8,
            atk: 20,
            hp: 300,
            status: 0,
            env: 'day',
            img: 'https://www.dmoe.cc/random.php',
            description: '豌豆射手哒'
        },
        {
            pid: 4,
            name: '豌豆射手',
            suns: 100,
            cd: 8,
            atk: 20,
            hp: 300,
            status: 0,
            env: 'day',
            img: 'https://www.dmoe.cc/random.php',
            description: '豌豆射手哒'
        }
    ]
}

const plantInfo = async () => {
    return {
        day: 12,
        night: 16
    }
}
export {
    listPlants,
    plantInfo
}
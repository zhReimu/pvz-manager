const getAssetsImageFile = (url: string) => {
    const path = `../assets/images/${url}`;
    //@ts-ignore
    const modules = import.meta.globEager("../assets/images/*");
    return modules[path].default;
}

export {
    getAssetsImageFile
}
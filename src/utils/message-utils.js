import { ElMessage } from 'element-plus'

const success = (msg) => {
    ElMessage.success(msg)
    delayReload()
}

const error = (msg) => {
    ElMessage.error(msg)
    delayReload()
}

const delayReload = () => {
    setTimeout(() => {
        window.location.reload()
    }, 1000)
}

export {
    success,
    error
}
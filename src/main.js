import './style.css'
import App from './App.vue'
import router from './router'
import * as Vue from 'vue'
import 'element-plus/dist/index.css'
import ElementPlus from 'element-plus'
import 'element-plus/theme-chalk/dark/css-vars.css'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'



const app = Vue.createApp(App)
app.use(ElementPlus, {
    locale: zhCn,
})
app.use(router)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.mount('#app')
